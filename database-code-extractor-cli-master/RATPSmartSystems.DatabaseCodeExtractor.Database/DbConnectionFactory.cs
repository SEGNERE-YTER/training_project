﻿using System.Data;
using System.Data.SqlClient;

namespace RATPSmartSystems.DatabaseCodeExtractor.Database;

/// <inheritdoc cref="IDbConnectionFactory"/>
public class DbConnectionFactory : IDbConnectionFactory
{
    private readonly string _connectionString;
    /// <summary>
    /// DbConnectionFactory constructor
    /// </summary>
    public DbConnectionFactory(string connectionString)
    {
        if (string.IsNullOrEmpty(connectionString))
            throw new ArgumentException("Database connection string cannot be null or empty");

        _connectionString = connectionString;
    }

    /// <inheritdoc/>
    public IDbConnection GetDbConnection()
    {
        var connection = new SqlConnection(_connectionString);
        connection.Open();
        return connection;
    }
}
