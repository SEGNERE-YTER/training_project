CREATE DATABASE [fakeDatabase]
GO

USE [fakeDatabase];
GO

EXEC('CREATE SCHEMA [fakeSchema]')
GO

CREATE TABLE [fakeSchema].[fakeTable] (
    Id INT NOT NULL IDENTITY,
    Name VARCHAR(20) NOT NULL,
);
GO

INSERT INTO [fakeSchema].[fakeTable] (Name)
VALUES 
('dummyObject'),
('dummyObject2');
GO

CREATE OR ALTER FUNCTION [fakeSchema].[fakeFunction]()
RETURNS VARCHAR(20)
AS
BEGIN    
    DECLARE @firstName VARCHAR(20);

    SET @firstName = (SELECT TOP 1 [Name] 
    FROM [fakeSchema].[fakeTable]);

    RETURN @firstName;
END
GO

CREATE OR ALTER PROCEDURE [fakeSchema].[fakeProcedure]
    @name VARCHAR(20)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT INTO [fakeSchema].[fakeTable]
([Name])
VALUES
    (@name)
END
GO

USE [msdb];
GO
EXEC dbo.sp_add_job
     @job_name = N'Weekly Sales Data Backup' ;
GO
EXEC sp_add_jobstep
     @job_name = N'Weekly Sales Data Backup',
     @step_name = N'Set database to read only',
     @subsystem = N'TSQL',
     @command = N'ALTER DATABASE SALES SET READ_ONLY',
     @retry_attempts = 5,
     @retry_interval = 5 ;
GO
EXEC dbo.sp_add_schedule
     @schedule_name = N'RunOnce',
     @freq_type = 1,
     @active_start_time = 233000 ;
USE msdb ;
GO
EXEC sp_attach_schedule
     @job_name = N'Weekly Sales Data Backup',
     @schedule_name = N'RunOnce';
GO
EXEC dbo.sp_add_jobserver
     @job_name = N'Weekly Sales Data Backup';
GO  
