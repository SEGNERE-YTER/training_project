using System.Data;

namespace RATPSmartSystems.DatabaseCodeExtractor.Database;

/// <summary>
/// Creates DbConnection objects for a database server.
/// </summary>
public interface IDbConnectionFactory
{
    /// <summary>
    /// Gets the database connection
    /// </summary>
    /// <returns>The DbConnection object</returns>
    IDbConnection GetDbConnection();
}
