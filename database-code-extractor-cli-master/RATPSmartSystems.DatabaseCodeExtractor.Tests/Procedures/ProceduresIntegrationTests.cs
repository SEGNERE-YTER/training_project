using System.Linq;
using System.Threading.Tasks;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using RATPSmartSystems.DatabaseCodeExtractor.Procedures;
using Shouldly;
using Xunit;

namespace RATPSmartSystems.DatabaseCodeExtractor.Tests.Procedures;

/// <summary>
/// Provides integration with containerized database for procedure creation scripts
/// </summary>
public class ProceduresIntegrationTests
{
    private const string ConnectionString = "Server=localhost, 1433;Database=master;User Id=sa;Password=password123!";
    private readonly IDbConnectionFactory _dbConnectionFactory;

    /// <summary>
    /// ProceduresIntegrationTests constructor
    /// </summary>
    public ProceduresIntegrationTests()
    {
        _dbConnectionFactory = new DbConnectionFactory(ConnectionString);
    }

    [Fact]
    public async Task GivenValidDatabaseWhenGetAllProceduresThenSucceed()
    {
        // arrange
        var procedureRepository = new ProceduresRepository(_dbConnectionFactory);

        // act
        var result = await procedureRepository.GetAllProceduresAsync("fakeDatabase").ConfigureAwait(false);

        // assert
        result.ShouldNotBeNull();
        var procedure = result.First(x => x.ProcedureName.Equals("fakeProcedure"));
        procedure.ShouldNotBeNull();
        procedure.DatabaseName.ShouldBe("fakeDatabase");
        procedure.SchemaName.ShouldBe("fakeSchema");
        procedure.Content.ShouldContain("INSERT INTO [fakeSchema].[fakeTable]");
    }
}
