using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using RATPSmartSystems.DatabaseCodeExtractor.Domain;
using RATPSmartSystems.DatabaseCodeExtractor.Gateways;
using RATPSmartSystems.DatabaseCodeExtractor.Procedures;
using Shouldly;
using Xunit;

namespace RATPSmartSystems.DatabaseCodeExtractor.Tests.Procedures;

/// <summary>
/// Provides unit tests for procedures creation scripts
/// </summary>
public class ProceduresUnitTests
{
    private const string FakeContent = @"CREATE OR ALTER PROCEDURE [fakeSchema].[fakeProcedure]
    @name VARCHAR(20)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    INSERT INTO [fakeSchema].[fakeTable]
    ([Name])
    VALUES
    (@name)
END";

    [Fact]
    public async Task GivenValidDatabaseNameWhenExtractProceduresThenSucceed()
    {
        // arrange
        var procedureRepository = new Mock<IProceduresRepository>();
        procedureRepository.Setup(x => x.GetAllProceduresAsync(It.IsAny<string>()))
            .ReturnsAsync(new List<Procedure>
            {
                new()
                {
                    DatabaseName = "fakeDatabase",
                    ProcedureName = "fakeProcedure",
                    SchemaName = "fakeSchema",
                    Content = FakeContent
                }
            });
        var storageGateway = new Mock<IStorageGateway>();
        var procedureService = new ProceduresService(procedureRepository.Object, storageGateway.Object);

        // act
        var result = await procedureService.ExtractProceduresAsync("fakeDatabase").ConfigureAwait(false);

        // assert
        result.ShouldNotBeNull();
        result.Count.ShouldBe(1);
        var resultProcedure = result.First();
        resultProcedure.DatabaseName.ShouldBe("fakeDatabase");
        resultProcedure.ProcedureName.ShouldBe("fakeProcedure");
        resultProcedure.SchemaName.ShouldBe("fakeSchema");
        resultProcedure.Content.ShouldBe(FakeContent);
    }
}
