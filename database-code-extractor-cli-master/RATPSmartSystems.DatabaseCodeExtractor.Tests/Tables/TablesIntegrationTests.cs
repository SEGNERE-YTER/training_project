using System.Linq;
using System.Threading.Tasks;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using RATPSmartSystems.DatabaseCodeExtractor.Tables;
using Shouldly;
using Xunit;

namespace RATPSmartSystems.DatabaseCodeExtractor.Tests.Tables;

/// <summary>
/// Provides integration with containerized database for tables creation scripts
/// </summary>
public class TablesIntegrationTests
{
    private const string ConnectionString = "Server=localhost, 1433;Database=master;User Id=sa;Password=password123!";
    private readonly IDbConnectionFactory _dbConnectionFactory;

    /// <summary>
    /// TablesIntegrationTests constructor
    /// </summary>
    public TablesIntegrationTests()
    {
        _dbConnectionFactory = new DbConnectionFactory(ConnectionString);
    }

    [Fact]
    public async Task GivenValidDatabaseWhenGetAllTablesThenSucceed()
    {
        // arrange
        var repository = new TablesRepository(_dbConnectionFactory);

        // act
        var result = await repository.GetAllTablesAsync("fakeDatabase").ConfigureAwait(false);

        // assert
        result.ShouldNotBeNull();
        result.Count.ShouldBe(1);
        var fakeTable = result.First();
        fakeTable.DatabaseName.ShouldBe("fakeDatabase");
        fakeTable.TableName.ShouldBe("fakeTable");
        fakeTable.SchemaName.ShouldBe("fakeSchema");
        fakeTable.CreationScript.ShouldNotBeNullOrEmpty();
    }
}
