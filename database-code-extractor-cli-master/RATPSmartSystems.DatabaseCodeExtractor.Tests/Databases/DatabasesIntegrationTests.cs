using System.Threading.Tasks;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using Xunit;
using RATPSmartSystems.DatabaseCodeExtractor.Databases;
using Shouldly;

namespace RATPSmartSystems.DatabaseCodeExtractor.Tests.Databases;

/// <summary>
/// Provides integration with containerized database for databases creation scripts
/// </summary>
public class DatabaseIntegrationTests
{
    private const string ConnectionString = "Server=localhost, 1433;Database=master;User Id=sa;Password=password123!";
    private readonly IDbConnectionFactory _dbConnectionFactory;

    /// <summary>
    /// DatabaseIntegrationTests constructor
    /// </summary>
    public DatabaseIntegrationTests()
    {
        _dbConnectionFactory = new DbConnectionFactory(ConnectionString);
    }

    [Fact]
    public async Task GivenValidConnectionStringWhenGetAllDatabasesThenDatabaseExtractionSucceed()
    {
        // arrange
        var repository = new DatabasesRepository(_dbConnectionFactory);

        // act
        var result = await repository.GetAllDatabasesAsync("fakeDatabase").ConfigureAwait(false);

        // assert
        result.ShouldNotBeNull();
        result.ShouldContain(x => x.DatabaseName == "fakeDatabase");
    }
}
