using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using RATPSmartSystems.DatabaseCodeExtractor.Domain;
using RATPSmartSystems.DatabaseCodeExtractor.Gateways;
using RATPSmartSystems.DatabaseCodeExtractor.Jobs;
using Shouldly;
using Xunit;

namespace RATPSmartSystems.DatabaseCodeExtractor.Tests.Jobs;

/// <summary>
/// Provides unit tests for jobs creation scripts
/// </summary>
public class JobsUnitTests
{
        [Fact]
    public async Task GivenValidContextWhenExtractJobsThenSucceed()
    {
        //  arrange
        var jobsRepository = new Mock<IJobsRepository>();
        var storageGateway = new Mock<IStorageGateway>();
        var jobsService = new JobsService(jobsRepository.Object, storageGateway.Object);
        jobsRepository.Setup(x => x.GetAllJobsAsync())
            .ReturnsAsync(new List<Job>
            {
                new()
                {
                    Name = "simpleJob",
                    Enabled = true,
                    Owner = "sa",
                    Description = "simple job description",
                    Category = "[Uncategorized (Local)]",
                    Steps = new List<JobStep>
                    {
                        new()
                        {
                            StepName = "PsTrt60MIN",
                            StepId = 1,
                            CommandExecutionSuccessCode = 0,
                            OnSuccessAction = 0,
                            OnSuccessStepId = 0,
                            OnFailAction = 2,
                            OnFailStepId = 0,
                            RetryAttempts = 0,
                            RetryInterval = 0,
                            OperatingSystemRunPriority = 0,
                            SubSystem = JobSubSystems.TSQL,
                            Command = "EXEC [BDSAE_Ps].[syst].[PsTrt60min]",
                            DatabaseName = "master",
                            Flags = 0
                        }
                    }
                },
                new()
                {
                    Name = "scheduledJob",
                    Enabled = true,
                    Owner = "sa",
                    Description = "scheduled job description",
                    Category = "[Uncategorized (Local)]",
                    Schedules = new List<JobSchedule>
                    {
                        new()
                        {
                            Enabled = true,
                            FrequenceInterval = 1,
                            FrequenceType = ScheduleFrequenceTypes.Daily,
                            FrequenceSubdayType = ScheduleFrequenceSubdayTypes.Hours,
                            FrequenceSubdayInterval = 1,
                            FrequenceRelativeInterval = FrequenceRelativeIntervalTypes.Unused,
                            FrequenceRecurrenceFactor = 0,
                            ActiveStartDate = 20190724,
                            ActiveEndDate = 99991231,
                            ActiveStartTime = 25000,
                            ActiveEndTime = 115000,
                            ScheduleUid = new Guid("8725cff2-5236-4324-8707-3f392976b0fc")
                        }
                    },
                    Steps = new List<JobStep>
                    {
                        new()
                        {
                            StepName = "PsTrt60MIN",
                            StepId = 1,
                            CommandExecutionSuccessCode = 0,
                            OnSuccessAction = 0,
                            OnSuccessStepId = 0,
                            OnFailAction = 2,
                            OnFailStepId = 0,
                            RetryAttempts = 0,
                            RetryInterval = 0,
                            OperatingSystemRunPriority = 0,
                            SubSystem = JobSubSystems.TSQL,
                            Command = "EXEC [BDSAE_Ps].[syst].[PsTrt60min]",
                            DatabaseName = "master",
                            Flags = 0
                        }
                    }
                },
                new()
                {
                    Name = "multiStepsJob",
                    Enabled = true,
                    Owner = "sa",
                    Description = "multi steps job description",
                    Category = "[Uncategorized (Local)]",
                    Steps = new List<JobStep>
                    {
                        new()
                        {
                            StepName = "PsTrt60MIN",
                            StepId = 1,
                            CommandExecutionSuccessCode = 0,
                            OnSuccessAction = 0,
                            OnSuccessStepId = 0,
                            OnFailAction = 2,
                            OnFailStepId = 0,
                            RetryAttempts = 0,
                            RetryInterval = 0,
                            OperatingSystemRunPriority = 0,
                            SubSystem = JobSubSystems.TSQL,
                            Command = "EXEC [BDSAE_Ps].[syst].[PsTrt60min]",
                            DatabaseName = "master",
                            Flags = 0
                        },
                        new()
                        {
                            StepName = "PsTrt60MIN2",
                            StepId = 2,
                            CommandExecutionSuccessCode = 0,
                            OnSuccessAction = 0,
                            OnSuccessStepId = 0,
                            OnFailAction = 2,
                            OnFailStepId = 0,
                            RetryAttempts = 0,
                            RetryInterval = 0,
                            OperatingSystemRunPriority = 0,
                            SubSystem = JobSubSystems.TSQL,
                            Command = "EXEC [BDSAE_Ps].[syst].[PsTrt60min2]",
                            DatabaseName = "master",
                            Flags = 0
                        }
                    }
                }
            });

        //  act
        var result = await jobsService.ExtractJobsAsync().ConfigureAwait(false);

        //  assert
        jobsRepository.Verify(x => x.GetAllJobsAsync(), Times.Once());
        result.ShouldNotBeNull();
        result.Count.ShouldBe(3);
        //  check a simple job
        var simpleJob = result[0];
        simpleJob.Name.ShouldBe("simpleJob");
        simpleJob.Enabled.ShouldBeTrue();
        simpleJob.Owner.ShouldBe("sa");
        simpleJob.Description.ShouldBe("simple job description");
        simpleJob.Category.ShouldBe("[Uncategorized (Local)]");
        simpleJob.Schedules.ShouldNotBeNull();
        simpleJob.Steps.ShouldNotBeNull();
        simpleJob.Steps.Count.ShouldBe(1);
        var step = simpleJob.Steps[0];
        step.StepName.ShouldBe("PsTrt60MIN");
        step.StepId.ShouldBe(1);
        step.CommandExecutionSuccessCode.ShouldBe(0);
        step.OnSuccessAction.ShouldBe(0);
        step.OnSuccessStepId.ShouldBe(0);
        step.OnFailAction.ShouldBe(2);
        step.OnFailStepId.ShouldBe(0);
        step.RetryAttempts.ShouldBe(0);
        step.RetryInterval.ShouldBe(0);
        step.OperatingSystemRunPriority.ShouldBe(0);
        step.SubSystem.ShouldBe(JobSubSystems.TSQL);
        step.Command.ShouldBe("EXEC [BDSAE_Ps].[syst].[PsTrt60min]");
        step.DatabaseName.ShouldBe("master");
        step.Flags.ShouldBe(0);
        // check a scheduled job
        var scheduledJob = result[1];
        scheduledJob.Name.ShouldBe("scheduledJob");
        scheduledJob.Enabled.ShouldBeTrue();
        scheduledJob.Owner.ShouldBe("sa");
        scheduledJob.Description.ShouldBe("scheduled job description");
        scheduledJob.Category.ShouldBe("[Uncategorized (Local)]");
        scheduledJob.Schedules.ShouldNotBeNull();
        scheduledJob.Schedules[0].Enabled.ShouldBeTrue();
        scheduledJob.Schedules[0].FrequenceInterval.ShouldBe(1);
        scheduledJob.Schedules[0].FrequenceType.ShouldBe(ScheduleFrequenceTypes.Daily);
        scheduledJob.Schedules[0].FrequenceSubdayType.ShouldBe(ScheduleFrequenceSubdayTypes.Hours);
        scheduledJob.Schedules[0].FrequenceSubdayInterval.ShouldBe(1);
        scheduledJob.Schedules[0].FrequenceRelativeInterval.ShouldBe(FrequenceRelativeIntervalTypes.Unused);
        scheduledJob.Schedules[0].FrequenceRecurrenceFactor.ShouldBe(0);
        scheduledJob.Schedules[0].ActiveStartDate.ShouldBe(20190724);
        scheduledJob.Schedules[0].ActiveEndDate.ShouldBe(99991231);
        scheduledJob.Schedules[0].ActiveStartTime.ShouldBe(25000);
        scheduledJob.Schedules[0].ActiveEndTime.ShouldBe(115000);
        scheduledJob.Schedules[0].ScheduleUid.ShouldBe(new Guid("8725cff2-5236-4324-8707-3f392976b0fc"));
        scheduledJob.Steps.ShouldNotBeNull();
        scheduledJob.Steps.Count.ShouldBe(1);
        var scheduledJobStep = scheduledJob.Steps[0];
        scheduledJobStep.StepName.ShouldBe("PsTrt60MIN");
        scheduledJobStep.StepId.ShouldBe(1);
        scheduledJobStep.CommandExecutionSuccessCode.ShouldBe(0);
        scheduledJobStep.OnSuccessAction.ShouldBe(0);
        scheduledJobStep.OnSuccessStepId.ShouldBe(0);
        scheduledJobStep.OnFailAction.ShouldBe(2);
        scheduledJobStep.OnFailStepId.ShouldBe(0);
        scheduledJobStep.RetryAttempts.ShouldBe(0);
        scheduledJobStep.RetryInterval.ShouldBe(0);
        scheduledJobStep.OperatingSystemRunPriority.ShouldBe(0);
        scheduledJobStep.SubSystem.ShouldBe(JobSubSystems.TSQL);
        scheduledJobStep.Command.ShouldBe("EXEC [BDSAE_Ps].[syst].[PsTrt60min]");
        scheduledJobStep.DatabaseName.ShouldBe("master");
        scheduledJobStep.Flags.ShouldBe(0);
        // check a job that contains multiple steps
        var multiStepsJob = result[2];
        multiStepsJob.Name.ShouldBe("multiStepsJob");
        multiStepsJob.Enabled.ShouldBeTrue();
        multiStepsJob.Owner.ShouldBe("sa");
        multiStepsJob.Description.ShouldBe("multi steps job description");
        multiStepsJob.Category.ShouldBe("[Uncategorized (Local)]");
        multiStepsJob.Schedules.ShouldNotBeNull();
        multiStepsJob.Steps.ShouldNotBeNull();
        multiStepsJob.Steps.Count.ShouldBe(2);
        var firstStep = multiStepsJob.Steps[0];
        firstStep.StepName.ShouldBe("PsTrt60MIN");
        firstStep.StepId.ShouldBe(1);
        firstStep.CommandExecutionSuccessCode.ShouldBe(0);
        firstStep.OnSuccessAction.ShouldBe(0);
        firstStep.OnSuccessStepId.ShouldBe(0);
        firstStep.OnFailAction.ShouldBe(2);
        firstStep.OnFailStepId.ShouldBe(0);
        firstStep.RetryAttempts.ShouldBe(0);
        firstStep.RetryInterval.ShouldBe(0);
        firstStep.OperatingSystemRunPriority.ShouldBe(0);
        firstStep.SubSystem.ShouldBe(JobSubSystems.TSQL);
        firstStep.Command.ShouldBe("EXEC [BDSAE_Ps].[syst].[PsTrt60min]");
        firstStep.DatabaseName.ShouldBe("master");
        firstStep.Flags.ShouldBe(0);
        var secondStep = multiStepsJob.Steps[1];
        secondStep.StepName.ShouldBe("PsTrt60MIN2");
        secondStep.StepId.ShouldBe(2);
        secondStep.CommandExecutionSuccessCode.ShouldBe(0);
        secondStep.OnSuccessAction.ShouldBe(0);
        secondStep.OnSuccessStepId.ShouldBe(0);
        secondStep.OnFailAction.ShouldBe(2);
        secondStep.OnFailStepId.ShouldBe(0);
        secondStep.RetryAttempts.ShouldBe(0);
        secondStep.RetryInterval.ShouldBe(0);
        secondStep.OperatingSystemRunPriority.ShouldBe(0);
        secondStep.SubSystem.ShouldBe(JobSubSystems.TSQL);
        secondStep.Command.ShouldBe("EXEC [BDSAE_Ps].[syst].[PsTrt60min2]");
        secondStep.DatabaseName.ShouldBe("master");
        secondStep.Flags.ShouldBe(0);
    }
}
