using System.Threading.Tasks;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using RATPSmartSystems.DatabaseCodeExtractor.Jobs;
using Shouldly;
using Xunit;

namespace RATPSmartSystems.DatabaseCodeExtractor.Tests.Jobs;

/// <summary>
/// Provides integration with containerized database for jobs creation scripts
/// </summary>
public class JobsIntegrationTests
{
    private const string ConnectionString = "Server=localhost, 1433;Database=master;User Id=sa;Password=password123!";
    private readonly IDbConnectionFactory _dbConnectionFactory;

    /// <summary>
    /// JobsIntegrationTests
    /// </summary>
    public JobsIntegrationTests()
    {
        _dbConnectionFactory = new DbConnectionFactory(ConnectionString);
    }

    [Fact]
    public async Task GivenNoContextWhenGetAllJobsThenSucceed()
    {
        // arrange

        var jobsRepository = new JobsRepository(_dbConnectionFactory);

        // act
        var result = await jobsRepository.GetAllJobsAsync().ConfigureAwait(false);

        // assert
        result.ShouldNotBeNull();
        result.Count.ShouldBe(1);
    }
}
