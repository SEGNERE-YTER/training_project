using System.Linq;
using System.Threading.Tasks;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using RATPSmartSystems.DatabaseCodeExtractor.Functions;
using Shouldly;
using Xunit;

namespace RATPSmartSystems.DatabaseCodeExtractor.Tests.Functions;

/// <summary>
/// Provides integration with containerized database for functions creation scripts
/// </summary>
public class FunctionsIntegrationTests
{
    private const string ConnectionString = "Server=localhost, 1433;Database=master;User Id=sa;Password=password123!";
    private readonly IDbConnectionFactory _dbConnectionFactory;

    /// <summary>
    /// FunctionsIntegrationTests constructor
    /// </summary>
    public FunctionsIntegrationTests()
    {
        _dbConnectionFactory = new DbConnectionFactory(ConnectionString);
    }

    [Fact]
    public async Task GivenWhenGetAllDatabasesThenDatabaseExtractionSucceed()
    {
        // arrange
        var repository = new FunctionsRepository(_dbConnectionFactory);

        // act
        var result = await repository.GetAllFunctionsAsync("fakeDatabase").ConfigureAwait(false);

        // assert
        result.ShouldNotBeNull();
        result.Count.ShouldBe(1);
        var fakeFunction = result.First();
        fakeFunction.DatabaseName.ShouldBe("fakeDatabase");
        fakeFunction.FunctionName.ShouldBe("fakeFunction");
        fakeFunction.SchemaName.ShouldBe("fakeSchema");
    }
}
