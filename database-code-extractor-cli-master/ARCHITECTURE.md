# database-code-extractor-cli architecture

Hey :wave:

This document describes the high-level architecture of our **database-code-extractor-cli**. If you want to familiarize yourself with the code base, you're just in the right place.

In order to share the architecture detail in a more visual way, we will use [**C4 modeling**](https://c4model.com/) that breakdowns the whole system into **four** different kinds of diagrams:

* Context
* Containers
* Components
* Code

Here, we will focus more on **Context** and **Containers** levels.

## System Context

```plantuml
@startuml Context diagram for db-extractor-cli 

!include <C4/C4_Context>

title Context diagram for db-extractor-cli 

skinparam linetype polyline

Person(developer, "Developer")

System(dbExtractor, "db-extractor-cli", "Extracts SQL code from an SQLServer database")

SystemDb_Ext(database, "SQLServer", "T-SQL")

Rel_R(developer, dbExtractor, "uses", "thru terminal")
Rel(dbExtractor, database, "sends queries")

@enduml

```

## Containers

```plantuml
@startuml Container diagram for db-extractor-cli 

!include <C4/C4_Container>

title Container diagram for db-extractor-cli 

skinparam linetype polyline

Person(developer, "Developer")

System_Boundary(dbExtractor, "db-extractor-cli"){
    Container(app, "DatabaseCodeExtractor.Console", "dotnet console app", "Exposes SQL code extraction thru command line interface")

    Container(featuresLibrary, "DatabaseCodeExtractor", "dotnet library", "Provides SQL code extraction features")
    Container(domainLibrary, "DatabaseCodeExtractor.Domain", "dotnet library", "Provides domain models")
    Container(databaseLibrary, "DatabaseCodeExtractor.Database", "dotnet library", "Provides database connection capabilites")
    Container(gatewaysLibrary, "DatabaseCodeExtractor.Gateways", "dotnet library", "Provides storage gateways features") 
}

ContainerDb_Ext(database, "SQLServer", "T-SQL")

Rel_R(developer, app, "uses", "thru terminal")
Rel(app, featuresLibrary, "uses")
Rel(featuresLibrary, domainLibrary, "uses")
Rel(featuresLibrary, databaseLibrary, "uses")
Rel(featuresLibrary, gatewaysLibrary, "uses")
Rel(databaseLibrary, database, "connects to")
Rel(featuresLibrary, database, "sends queries")

@enduml
```
