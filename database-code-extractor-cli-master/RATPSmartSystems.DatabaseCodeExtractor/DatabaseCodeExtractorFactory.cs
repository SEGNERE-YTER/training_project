﻿using RATPSmartSystems.DatabaseCodeExtractor.Database;
using RATPSmartSystems.DatabaseCodeExtractor.Databases;
using RATPSmartSystems.DatabaseCodeExtractor.Functions;
using RATPSmartSystems.DatabaseCodeExtractor.Gateways;
using RATPSmartSystems.DatabaseCodeExtractor.Jobs;
using RATPSmartSystems.DatabaseCodeExtractor.Permissions;
using RATPSmartSystems.DatabaseCodeExtractor.Procedures;
using RATPSmartSystems.DatabaseCodeExtractor.Schemas;
using RATPSmartSystems.DatabaseCodeExtractor.ServerLinks;
using RATPSmartSystems.DatabaseCodeExtractor.Tables;
using RATPSmartSystems.DatabaseCodeExtractor.Triggers;

namespace RATPSmartSystems.DatabaseCodeExtractor;

/// <summary>
/// Provides creation methods for database extraction
/// </summary>
public static class DatabaseCodeExtractorFactory
{
    /// <summary>
    /// Creates database connection string factory
    /// </summary>
    /// <param name="connectionString"></param>
    /// <returns></returns>
    public static IDbConnectionFactory CreateDbConnectionFactory(string connectionString)
    {
        return new DbConnectionFactory(connectionString);
    }

    /// <summary>
    /// Creates database extraction component
    /// </summary>
    /// <param name="connectionFactory">The database connection factory</param>
    /// <returns>The database extraction component</returns>
    public static IDatabasesService CreateDatabasesService(IDbConnectionFactory connectionFactory)
    {
        var repository = new DatabasesRepository(connectionFactory);
        var storageGateway = new StorageGateway();
        var service = new DatabasesService(repository, storageGateway);

        return service;
    }

    /// <summary>
    /// Creates function extraction component
    /// </summary>
    /// <param name="connectionFactory">The database connection factory</param>
    /// <returns>The function extraction component</returns>
    public static IFunctionsService CreateFunctionsService(IDbConnectionFactory connectionFactory)
    {
        var repository = new FunctionsRepository(connectionFactory);
        var storageGateway = new StorageGateway();
        var service = new FunctionsService(repository, storageGateway);

        return service;
    }

    /// <summary>
    /// Creates job extraction component
    /// </summary>
    /// <param name="connectionFactory">The database connection factory</param>
    /// <returns>The job extraction component</returns>
    /// <exception cref="NotImplementedException"></exception>
    public static IJobsService CreateJobsService(IDbConnectionFactory connectionFactory)
    {
        var repository = new JobsRepository(connectionFactory);
        var storageGateway = new StorageGateway();
        var service = new JobsService(repository, storageGateway);

        return service;
    }

    /// <summary>
    /// Creates permission extraction component
    /// </summary>
    /// <param name="connectionString">The database connection string</param>
    /// <returns>The permission extraction component</returns>
    /// <exception cref="NotImplementedException"></exception>
    public static IPermissionsService CreatePermissionsService(string connectionString)
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// Creates procedure extraction component
    /// </summary>
    /// <param name="connectionFactory">The database connection factory</param>
    /// <returns>The procedure extraction component</returns>
    public static IProceduresService CreateProceduresService(IDbConnectionFactory connectionFactory)
    {
        var repository = new ProceduresRepository(connectionFactory);
        var storageGateway = new StorageGateway();
        var service = new ProceduresService(repository, storageGateway);

        return service;
    }

    /// <summary>
    /// Creates schema extraction component
    /// </summary>
    /// <param name="connectionFactory">The database connection factory</param>
    /// <returns>The schema extraction component</returns>
    public static ISchemasService CreateSchemasService(IDbConnectionFactory connectionFactory)
    {
        var repository = new SchemasRepository(connectionFactory);
        var storageGateway = new StorageGateway();
        var service = new SchemasService(repository, storageGateway);

        return service;
    }

    /// <summary>
    /// Creates server link extraction component
    /// </summary>
    /// <param name="connectionString">The database connection string</param>
    /// <returns>The server link extraction component</returns>
    /// <exception cref="NotImplementedException"></exception>
    public static IServerLinksService CreateServerLinksService(string connectionString)
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// Creates table extraction component
    /// </summary>
    /// <param name="connectionFactory">The database connection factory</param>
    /// <returns>The table extraction component</returns>
    public static ITablesService CreateTablesService(IDbConnectionFactory connectionFactory)
    {
        var repository = new TablesRepository(connectionFactory);
        var storageGateway = new StorageGateway();
        var service = new TablesService(repository, storageGateway);

        return service;
    }

    /// <summary>
    /// Creates triggers extraction component
    /// </summary>
    /// <param name="connectionFactory">The database connection factory</param>
    /// <returns>The triggers extraction component</returns>
    /// <exception cref="NotImplementedException"></exception>
    public static ITriggersService CreateTriggersService(IDbConnectionFactory connectionFactory)
    {
        var repository = new TriggersRepository(connectionFactory);
        var storageGateway = new StorageGateway();
        var service = new TriggersService(repository, storageGateway);

        return service;
    }

}
