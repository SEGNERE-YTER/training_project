using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Procedures;

/// <summary>
/// Provides persistence features about SQL procedures
/// </summary>
public interface IProceduresRepository
{
    /// <summary>
    /// Gets all procedures from a database
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>The list of the procedures</returns>
    Task<IList<Procedure>> GetAllProceduresAsync(string databaseName);
}
