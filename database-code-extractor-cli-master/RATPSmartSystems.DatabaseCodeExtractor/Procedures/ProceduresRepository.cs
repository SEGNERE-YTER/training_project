using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using Dapper;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using RATPSmartSystems.DatabaseCodeExtractor.Domain;

[assembly: InternalsVisibleTo("RATPSmartSystems.DatabaseCodeExtractor.Tests")]
namespace RATPSmartSystems.DatabaseCodeExtractor.Procedures;

/// <inheritdoc cref="IProceduresRepository"/>
internal class ProceduresRepository : IProceduresRepository
{
    private readonly IDbConnectionFactory _connectionFactory;
    private const string CreationPattern = @"CREATE\s{1,}procedure";
    private const string CreateOrAlter = "CREATE OR ALTER PROCEDURE";

    /// <summary>
    /// ProceduresRepository constructor
    /// </summary>
    public ProceduresRepository(IDbConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }

    /// <inheritdoc/>
    public async Task<IList<Procedure>> GetAllProceduresAsync(string databaseName)
    {
        var result = new List<Procedure>();

        try
        {
            if (!string.IsNullOrEmpty(databaseName))
            {
                var procedures = await GetAllProceduresByDatabaseAsync(databaseName).ConfigureAwait(false);
                result = procedures.ToList();
            }
            else
            {
                var dbNames = await GetDatabaseNamesAsync().ConfigureAwait(false);

                foreach (var dbName in dbNames)
                {
                    var procedures = await GetAllProceduresByDatabaseAsync(dbName).ConfigureAwait(false);
                    result.AddRange(procedures.ToList());
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
        return result;
    }


    private async Task<IList<Procedure>> GetAllProceduresByDatabaseAsync(string database)
    {
        var query = @$"SELECT
    '{database}' AS [DatabaseName],
    OBJECT_NAME(p.object_id) AS [ProcedureName],
	SCHEMA_NAME(p.schema_id) as [SchemaName],
    m.definition AS [Content]
FROM [{database}].[sys].[procedures] p
INNER JOIN [{database}].[sys].[sql_modules] m ON m.object_id = p.object_id
WHERE type != 'PC'
AND OBJECT_NAME(p.object_id) != 'GetTableCreationScript'
AND SCHEMA_NAME(p.schema_id) != 'oqs'
AND [name] NOT LIKE 'sp_%'";

        using var connection = _connectionFactory.GetDbConnection();
        var procedures = await connection.QueryAsync<Procedure>(query)
            .ConfigureAwait(false);
        
        foreach (var procedure in procedures)
        {
            var result = Regex.Replace(procedure.Content,
                CreationPattern,
                CreateOrAlter,
                RegexOptions.IgnoreCase);
            procedure.Content = result;
        }  
        return procedures.ToList();
        
    }
    
    private async Task<IList<string>> GetDatabaseNamesAsync()
    {
        var query = @"SELECT [Name] as [DatabaseName]
FROM [sys].[databases]
WHERE [Name] NOT IN ('master', 'tempdb', 'model', 'msdb');";

        using var connection = _connectionFactory.GetDbConnection();

        var databases = await connection.QueryAsync<string>(query)
            .ConfigureAwait(false);

        return databases.ToList();
    }
}
