using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Procedures;

/// <summary>
/// Provides extraction methods for procedures
/// </summary>
public interface IProceduresService
{
    /// <summary>
    /// Extracts all procedures from a database
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>The list containing the procedures info</returns>
    Task<IList<Procedure>> ExtractProceduresAsync(string databaseName);

    /// <summary>
    /// Extracts all procedures from a database and stores it into files
    /// </summary>
    /// <param name="destinationPath">The destination path for procedure files</param>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>True if succeed; False otherwise</returns>
    Task<bool> ExtractProceduresToFileAsync(string destinationPath, string databaseName);
}
