using System.Text;
using RATPSmartSystems.DatabaseCodeExtractor.Domain;
using RATPSmartSystems.DatabaseCodeExtractor.Gateways;

namespace RATPSmartSystems.DatabaseCodeExtractor.Procedures;

/// <inheritdoc cref="IProceduresService"/>
internal class ProceduresService : IProceduresService
{
    private readonly IProceduresRepository _proceduresRepository;
    private readonly IStorageGateway _storageGateway;

    /// <summary>
    /// ProceduresService constructor
    /// </summary>
    public ProceduresService(IProceduresRepository proceduresRepository,
        IStorageGateway storageGateway)
    {
        _proceduresRepository = proceduresRepository;
        _storageGateway = storageGateway;
    }

    /// <inheritdoc/>
    public Task<IList<Procedure>> ExtractProceduresAsync(string databaseName)
    {
        return _proceduresRepository.GetAllProceduresAsync(databaseName);
    }

    /// <inheritdoc/>
    public async Task<bool> ExtractProceduresToFileAsync(string destinationPath, string databaseName)
    {
        var procedures = await ExtractProceduresAsync(databaseName)
            .ConfigureAwait(false);
        var result = true;
        const char separator = '.';
        const string sqlExtension = ".sql";
        const string procedureFolder = "05-procedures";
        foreach (var p in procedures)
        {
            var dbName = p.DatabaseName.ToLowerInvariant();
            var fileName = new StringBuilder(dbName)
                .Append(separator)
                .Append(p.SchemaName)
                .Append(separator)
                .Append(p.ProcedureName)
                .Append(sqlExtension)
                .ToString();
            
            var path = Path.Combine(destinationPath, dbName, procedureFolder, fileName);
            result &= await _storageGateway.SaveContent(path, p.Content)
                .ConfigureAwait(false);
        }

        return result;
    }
}
