using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Schemas;

/// <summary>
/// Provides persistence features about SQL schemas
/// </summary>
public interface ISchemasRepository
{
    /// <summary>
    /// Gets all the schemas from database
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>The list of the schemas</returns>
    Task<IList<Schema>> GetAllSchemasAsync(string databaseName);
}
