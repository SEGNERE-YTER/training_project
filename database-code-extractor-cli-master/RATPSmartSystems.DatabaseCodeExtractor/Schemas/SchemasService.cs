using RATPSmartSystems.DatabaseCodeExtractor.Domain;
using RATPSmartSystems.DatabaseCodeExtractor.Gateways;

namespace RATPSmartSystems.DatabaseCodeExtractor.Schemas;

/// <inheritdoc cref="ISchemasService"/>
internal class SchemasService : ISchemasService
{
    private readonly ISchemasRepository _schemasRepository;
    private readonly IStorageGateway _storageGateway;

    /// <summary>
    /// SchemasService constructor
    /// </summary>
    public SchemasService(ISchemasRepository schemasRepository,
        IStorageGateway storageGateway)
    {
        _schemasRepository = schemasRepository;
        _storageGateway = storageGateway;
    }

    /// <inheritdoc/>
    public Task<IList<Schema>> ExtractSchemasAsync(string databaseName)
    {
        return _schemasRepository.GetAllSchemasAsync(databaseName);
    }

    public async Task<bool> ExtractSchemasToFileAsync(string destinationPath, string databaseName)
    {
        var schemas = await ExtractSchemasAsync(databaseName)
            .ConfigureAwait(false);
        var result = true;
        const string schemaFolder = "01-schemas";

        foreach (var s in schemas)
        {
            var dbName = s.DatabaseName.ToLowerInvariant();
            var path = Path.Combine(destinationPath, dbName, schemaFolder, $"{s.DatabaseName}.{s.SchemaName}.sql");
            result &= await _storageGateway.SaveContent(path, s.ToString())
                .ConfigureAwait(false);
        }

        return result;
    }
}
