using System.Runtime.CompilerServices;
using Dapper;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using RATPSmartSystems.DatabaseCodeExtractor.Domain;

[assembly: InternalsVisibleTo("RATPSmartSystems.DatabaseCodeExtractor.Tests")]
namespace RATPSmartSystems.DatabaseCodeExtractor.Schemas;

/// <inheritdoc cref="ISchemasRepository"/>
internal class SchemasRepository : ISchemasRepository
{
    private readonly IDbConnectionFactory _connectionFactory;

    /// <summary>
    /// SchemasRepository constructor
    /// </summary>
    public SchemasRepository(IDbConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }

    /// <inheritdoc/>
    public async Task<IList<Schema>> GetAllSchemasAsync(string databaseName)
    {
        var result = new List<Schema>();

        async Task GetAllSchemasByDatabaseAsync(string database)
        {
            try
            {
                var query = @"SELECT
s.name AS [SchemaName],
u.name AS [SchemaOwner],
DB_NAME() AS [DatabaseName]
FROM
sys.schemas s
INNER JOIN sys.sysusers u ON u.uid = s.principal_id
WHERE [u].[hasdbaccess] = 1
ORDER BY s.name;";
                using var connection = _connectionFactory.GetDbConnection();

                connection.ChangeDatabase(database);

                var schemas = await connection.QueryAsync<Schema>(query)
                    .ConfigureAwait(false);

                result.AddRange(schemas.ToList());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        if (!string.IsNullOrEmpty(databaseName))
        {
            await GetAllSchemasByDatabaseAsync(databaseName)
                .ConfigureAwait(false);
        }
        else
        {
            var databaseNames = await GetDatabaseNamesAsync()
                .ConfigureAwait(false);
            foreach (var dbName in databaseNames)
            {
                await GetAllSchemasByDatabaseAsync(dbName)
                    .ConfigureAwait(false);
            }
        }


        return result;
    }

    private async Task<IList<string>> GetDatabaseNamesAsync()
    {
        var query = @"SELECT [Name] as [DatabaseName]
FROM [sys].[databases]
WHERE [Name] NOT IN ('master', 'tempdb', 'model', 'msdb');";

        using var connection = _connectionFactory.GetDbConnection();

        var databases = await connection.QueryAsync<string>(query)
            .ConfigureAwait(false);

        return databases.ToList();
    }
}
