using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Schemas;

/// <summary>
/// Provides extraction methods for schemas
/// </summary>
public interface ISchemasService
{
    /// <summary>
    /// Extracts all schemas from a database
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>The list containing the schemas info</returns>
    Task<IList<Schema>> ExtractSchemasAsync(string databaseName);

    /// <summary>
    /// Extracts all schemas from a database and stores it into files
    /// </summary>
    /// <param name="destinationPath">The destination path for schema files</param>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>True if succeed; False otherwise</returns>
    Task<bool> ExtractSchemasToFileAsync(string destinationPath, string databaseName);
}
