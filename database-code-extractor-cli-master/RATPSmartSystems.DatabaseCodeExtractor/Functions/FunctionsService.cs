using System.Text;
using RATPSmartSystems.DatabaseCodeExtractor.Domain;
using RATPSmartSystems.DatabaseCodeExtractor.Gateways;

namespace RATPSmartSystems.DatabaseCodeExtractor.Functions;

/// <inheritdoc cref="IFunctionsService"/>
public class FunctionsService : IFunctionsService
{
    private readonly IFunctionsRepository _functionsRepository;
    private readonly IStorageGateway _storageGateway;
    /// <summary>
    /// FunctionsService constructor
    /// </summary>
    public FunctionsService(IFunctionsRepository functionsRepository,
        IStorageGateway storageGateway)
    {
        _functionsRepository = functionsRepository;
        _storageGateway = storageGateway;
    }

    /// <inheritdoc/>
    public Task<IList<Function>> ExtractFunctionsAsync(string databaseName)
    {
        return _functionsRepository.GetAllFunctionsAsync(databaseName);
    }

    /// <inheritdoc/>
    public async Task<bool> ExtractFunctionsToFileAsync(string destinationPath, string databaseName)
    {
        var functions = await ExtractFunctionsAsync(databaseName)
            .ConfigureAwait(false);
        var result = true;
        const char separator = '.';
        const string sqlExtension = ".sql";
        const string functionFolder = "04-functions";
        foreach (var f in functions)
        {
            var dbName = f.DatabaseName.ToLowerInvariant();
            var fileName = new StringBuilder(dbName)
                .Append(separator)
                .Append(f.SchemaName)
                .Append(separator)
                .Append(f.FunctionName)
                .Append(sqlExtension)
                .ToString();

            var path = Path.Combine(destinationPath, dbName, functionFolder, fileName);
            result &= await _storageGateway.SaveContent(path, f.ToString())
                .ConfigureAwait(false);
        }

        return result;
    }
}
