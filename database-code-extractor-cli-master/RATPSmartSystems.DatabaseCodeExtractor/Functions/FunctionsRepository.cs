using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using Dapper;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using RATPSmartSystems.DatabaseCodeExtractor.Domain;

[assembly: InternalsVisibleTo("RATPSmartSystems.DatabaseCodeExtractor.Tests")]
namespace RATPSmartSystems.DatabaseCodeExtractor.Functions;

/// <inheritdoc cref="IFunctionsRepository"/>
internal class FunctionsRepository : IFunctionsRepository
{
    private readonly IDbConnectionFactory _connectionFactory;
    private const string CreationPattern = @"CREATE\s{1,}function";
    private const string CreateOrAlter = "CREATE OR ALTER FUNCTION";

    /// <summary>
    /// FunctionRepository constructor
    /// </summary>
    public FunctionsRepository(IDbConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }

    /// <inheritdoc/>
    public async Task<IList<Function>> GetAllFunctionsAsync(string databaseName)
    {
        var result = new List<Function>();
        try
        {
            if (!string.IsNullOrEmpty(databaseName))
            {
                var functions = await GetFunctionsByDatabaseAsync(databaseName).ConfigureAwait(false);
                result = functions.ToList();
            }
            else
            {
                var dbNames = await GetDatabaseNamesAsync()
                    .ConfigureAwait(false);

                foreach (var dbName in dbNames)
                {
                    var procedures = await GetFunctionsByDatabaseAsync(dbName).ConfigureAwait(false);
                    result.AddRange(procedures.ToList());                  

                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

        return result;
    }

    private async Task<IList<Function>> GetFunctionsByDatabaseAsync(string database)
    {
        var query = $@"SELECT
'{database}' AS [DatabaseName]
, o.[name] AS [FunctionName]
, SCHEMA_NAME(o.schema_id) AS [SchemaName]
, m.definition AS [Content]
FROM sys.objects o
INNER JOIN sys.sql_modules m ON m.object_id = o.object_id
WHERE type in ('FN', 'IF', 'TF');";

        using var connection = _connectionFactory.GetDbConnection();
        connection.ChangeDatabase(database);

        var functions = await connection.QueryAsync<Function>(query)
            .ConfigureAwait(false);
        foreach (var function in functions)
        {
            var result = Regex.Replace(function.Content,
                CreationPattern,
                CreateOrAlter,
                RegexOptions.IgnoreCase);
            function.Content = result;
        }

        return functions.ToList();
    }
    private async Task<IList<string>> GetDatabaseNamesAsync()
    {
        var query = @"SELECT [Name] as [DatabaseName]
FROM [sys].[databases]
WHERE [Name] NOT IN ('master', 'tempdb', 'model', 'msdb');";

        using var connection = _connectionFactory.GetDbConnection();

        var databases = await connection.QueryAsync<string>(query)
            .ConfigureAwait(false);

        return databases.ToList();
    }
}
