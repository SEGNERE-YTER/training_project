using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Functions;

/// <summary>
/// Provides extraction methods for user-defined functions
/// </summary>
public interface IFunctionsService
{
    /// <summary>
    /// Extracts all user-defined functions from a database
    /// </summary>
    /// <returns>The list containing the functions info</returns>
    Task<IList<Function>> ExtractFunctionsAsync(string databaseName);

    /// <summary>
    /// Extracts all user-defined functions from a database and stores it into files
    /// </summary>
    /// <param name="destinationPath">The destination path for function files</param>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>True if succeed; False otherwise</returns>
    Task<bool> ExtractFunctionsToFileAsync(string destinationPath, string databaseName);
}
