using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Functions;

/// <summary>
/// Provides persistence features about SQL functions
/// </summary>
public interface IFunctionsRepository
{
    /// <summary>
    /// Gets all user-defined functions for a specific database
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns></returns>
    Task<IList<Function>> GetAllFunctionsAsync(string databaseName);
}
