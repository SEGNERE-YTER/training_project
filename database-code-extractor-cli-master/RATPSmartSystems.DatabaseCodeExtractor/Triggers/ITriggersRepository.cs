using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Triggers;

/// <summary>
/// Provides persistence features about triggers
/// </summary>
public interface ITriggersRepository
{
    /// <summary>
    /// Gets all the triggers from database
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>The list of the tables</returns>
    Task<IList<Trigger>> GetAllTriggersAsync(string databaseName);
}
