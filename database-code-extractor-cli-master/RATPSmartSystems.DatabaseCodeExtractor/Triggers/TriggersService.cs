using System.Text;
using RATPSmartSystems.DatabaseCodeExtractor.Domain;
using RATPSmartSystems.DatabaseCodeExtractor.Gateways;

namespace RATPSmartSystems.DatabaseCodeExtractor.Triggers;

/// <inheritdoc cref="ITriggersService"/>
internal class TriggersService : ITriggersService
{
    private readonly ITriggersRepository _triggersRepository;
    private readonly IStorageGateway _storageGateway;

    /// <summary>
    /// TriggersService constructor
    /// </summary>
    public TriggersService(ITriggersRepository triggersRepository,
        IStorageGateway storageGateway)
    {
        _triggersRepository = triggersRepository;
        _storageGateway = storageGateway;
    }

    /// <inheritdoc/>
    public Task<IList<Trigger>> ExtractTriggersAsync(string databaseName)
    {
        return _triggersRepository.GetAllTriggersAsync(databaseName);
    }

    /// <inheritdoc/>
    public async Task<bool> ExtractTriggersToFileAsync(string destinationPath, string databaseName)
    {
        var triggers = await ExtractTriggersAsync(databaseName)
            .ConfigureAwait(false);
        var result = true;
        const string triggersFolder = "06-triggers";
        foreach (var t in triggers)
        {
            var dbName = t.DatabaseName.ToLowerInvariant();
            const char separator = '.';
            var fileName = new StringBuilder(dbName)
                .Append(separator)
                .Append(string.IsNullOrEmpty(t.SchemaName) ? string.Empty: t.SchemaName + separator)
                .Append(t.TriggerName)
                .Append(".sql").ToString();

            var path = Path.Combine(destinationPath, dbName, triggersFolder, fileName);
            result &= await _storageGateway.SaveContent(path, t.ToString())
                .ConfigureAwait(false);
        }

        return result;
    }
}
