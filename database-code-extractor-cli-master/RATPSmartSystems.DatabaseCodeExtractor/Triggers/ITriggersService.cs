using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Triggers;

/// <summary>
/// Provides extraction methods for triggers
/// </summary>
public interface ITriggersService
{
    /// <summary>
    /// Extracts all triggers from a database
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>The list containing the triggers info</returns>
    Task<IList<Trigger>> ExtractTriggersAsync(string databaseName);

    /// <summary>
    /// Extracts all triggers from a database and stores it into files
    /// </summary>
    /// <param name="destinationPath">The destination path for schema files</param>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>True if succeed; False otherwise</returns>
    Task<bool> ExtractTriggersToFileAsync(string destinationPath, string databaseName);
}
