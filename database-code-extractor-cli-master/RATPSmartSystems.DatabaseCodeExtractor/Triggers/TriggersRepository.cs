using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Dapper;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using RATPSmartSystems.DatabaseCodeExtractor.Domain;

[assembly: InternalsVisibleTo("RATPSmartSystems.DatabaseCodeExtractor.Tests")]
namespace RATPSmartSystems.DatabaseCodeExtractor.Triggers;

/// <inheritdoc cref="ITriggersRepository"/>
internal class TriggersRepository : ITriggersRepository
{
    private readonly IDbConnectionFactory _connectionFactory;
    private const string TriggerCreationPattern = @"CREATE\s{1,}trigger";
    private const string CreateOrAlter = "CREATE OR ALTER TRIGGER";

    /// <summary>
    /// TriggersRepository constructor
    /// </summary>
    public TriggersRepository(IDbConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }

    /// <inheritdoc/>
    public async Task<IList<Trigger>> GetAllTriggersAsync(string databaseName)
    {
        try
        {
            if (!string.IsNullOrEmpty(databaseName))
            {
                return await GetAllTriggersByDatabaseName(databaseName)
                    .ConfigureAwait(false);
            }
            else
            {
                var result = new List<Trigger>();
                var dbNames = await GetDatabaseNamesAsync()
                    .ConfigureAwait(false);

                foreach (var dbName in dbNames)
                {
                    result.AddRange(await GetAllTriggersByDatabaseName(dbName)
                        .ConfigureAwait(false));
                }

                return result;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return new List<Trigger>();
        }
    }

    private async Task<IList<Trigger>> GetAllTriggersByDatabaseName(string databaseName)
    {
        var query = @$"SELECT
'{databaseName}' AS [DatabaseName],
[name] AS [TriggerName],
OBJECT_SCHEMA_NAME(t.object_id) AS [SchemaName],
m.definition AS [Content]
FROM sys.triggers t
INNER JOIN sys.sql_modules m ON m.object_id = t.object_id
WHERE type ='TR'";
        using var connection = _connectionFactory.GetDbConnection();
        connection.ChangeDatabase(databaseName);

        var triggers = await connection.QueryAsync<Trigger>(query)
            .ConfigureAwait(false);
        foreach (var trigger in triggers)
        {
            var result = Regex.Replace(trigger.Content,
                TriggerCreationPattern,
                CreateOrAlter,
                RegexOptions.IgnoreCase);
            trigger.Content = result;
        }
        return triggers.ToList();

    }
     private async Task<IList<string>> GetDatabaseNamesAsync()
    {
        var query = @"SELECT [Name] as [DatabaseName]
FROM [sys].[databases]
WHERE [Name] NOT IN ('master', 'tempdb', 'model', 'msdb');";

        using var connection = _connectionFactory.GetDbConnection();

        var databases = await connection.QueryAsync<string>(query)
            .ConfigureAwait(false);

        return databases.ToList();
    }
}
