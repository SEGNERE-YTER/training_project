using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.ServerLinks;

/// <summary>
/// Provides extraction methods for server links
/// </summary>
public interface IServerLinksService
{
    /// <summary>
    /// Extracts all server links from a database
    /// </summary>
    /// <returns>The list containing the server links info</returns>
    Task<IList<ServerLink>> ExtractServerLinksAsync();

    /// <summary>
    /// Extracts all server links from a database and stores it into files
    /// </summary>
    /// <param name="destinationPath">The destination path for schema files</param>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>True if succeed; False otherwise</returns>
    Task<bool> ExtractServerLinksToFileAsync(string destinationPath, string databaseName);
}
