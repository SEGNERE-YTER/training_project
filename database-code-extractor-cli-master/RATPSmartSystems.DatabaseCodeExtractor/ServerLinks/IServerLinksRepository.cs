using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.ServerLinks;

/// <summary>
/// Provides persistence features about server links
/// </summary>
public interface IServerLinksRepository
{
    /// <summary>
    /// Gets all the server links from a database
    /// </summary>
    /// <returns>The list of the server links</returns>
    Task<IList<ServerLink>> GetAllServerLinksAsync();
}
