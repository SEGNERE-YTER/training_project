using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Permissions;

/// <summary>
/// Provides persistence features about database users, roles and grants
/// </summary>
public interface IPermissionsRepository
{
    /// <summary>
    /// Gets all SQL permissions for a specific database
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>The list of SQL permissions</returns>
    Task<IList<Permission>> GetAllPermissionsAsync(string databaseName);
}
