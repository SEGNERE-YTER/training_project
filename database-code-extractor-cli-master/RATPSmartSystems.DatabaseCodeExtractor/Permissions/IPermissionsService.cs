using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Permissions;

/// <summary>
/// Provides extraction methods for permissions
/// </summary>
public interface IPermissionsService
{
    /// <summary>
    /// Extracts all permissions from a database
    /// </summary>
    /// <returns></returns>
    Task<IList<Permission>> ExtractPermissionsAsync();

    /// <summary>
    /// Extracts all permissions from a database and stores it into files
    /// </summary>
    /// <param name="destinationPath">The destination path for job files</param>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>True if succeed; False otherwise</returns>
    Task<bool> ExtractPermissionsToFileAsync(string destinationPath, string databaseName);
}
