using RATPSmartSystems.DatabaseCodeExtractor.Gateways;

namespace RATPSmartSystems.DatabaseCodeExtractor.Databases;

/// <inheritdoc cref="IDatabasesService"/>
public class DatabasesService : IDatabasesService
{
    private readonly IDatabasesRepository _databasesRepository;
    private readonly IStorageGateway _storageGateway;
    /// <summary>
    /// DatabasesService constructor
    /// </summary>
    public DatabasesService(IDatabasesRepository databasesRepository,
        IStorageGateway storageGateway)
    {
        _databasesRepository = databasesRepository;
        _storageGateway = storageGateway;
    }

    /// <inheritdoc/>
    public Task<IList<Domain.Database>> ExtractDatabasesAsync(string databaseName)
    {
        return _databasesRepository.GetAllDatabasesAsync(databaseName);
    }

    /// <inheritdoc/>
    public async  Task<bool> ExtractDatabasesToFileAsync(string destinationPath, string databaseName)
    {
        var databases = await ExtractDatabasesAsync(databaseName)
            .ConfigureAwait(false);
        var result = true;

        foreach (var d in databases)
        {
            var lowerName = d.DatabaseName.ToLowerInvariant();
            var path = Path.Combine(destinationPath, lowerName, $"init-{lowerName}.sql");
            result &= await _storageGateway.SaveContent(path, d.ToString())
                .ConfigureAwait(false);
        }

        return result;

    }
}
