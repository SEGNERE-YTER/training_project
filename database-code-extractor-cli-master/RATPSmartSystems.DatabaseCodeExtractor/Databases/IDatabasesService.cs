namespace RATPSmartSystems.DatabaseCodeExtractor.Databases;

/// <summary>
/// Provides extraction methods for databases
/// </summary>
public interface IDatabasesService
{
    /// <summary>
    /// Extracts all databases
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>The list containing the functions info</returns>
    Task<IList<Domain.Database>> ExtractDatabasesAsync(string databaseName);

    /// <summary>
    /// Extracts all databases and stores it into files
    /// </summary>
    /// <param name="destinationPath">The destination path for function files</param>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>True if succeed; False otherwise</returns>
    Task<bool> ExtractDatabasesToFileAsync(string destinationPath, string databaseName);
}
