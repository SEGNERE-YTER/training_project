namespace RATPSmartSystems.DatabaseCodeExtractor.Databases;

/// <summary>
/// Provides persistence features about SQL databases
/// </summary>
public interface IDatabasesRepository
{
    /// <summary>
    /// Gets all user-defined functions for a specific database
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns></returns>
    Task<IList<Domain.Database>> GetAllDatabasesAsync(string databaseName);
}
