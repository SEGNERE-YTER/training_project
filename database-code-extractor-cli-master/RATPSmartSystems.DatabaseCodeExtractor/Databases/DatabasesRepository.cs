using System.Runtime.CompilerServices;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using Dapper;

[assembly: InternalsVisibleTo("RATPSmartSystems.DatabaseCodeExtractor.Tests")]
namespace RATPSmartSystems.DatabaseCodeExtractor.Databases;

/// <inheritdoc cref="IDatabasesRepository"/>
internal class DatabasesRepository : IDatabasesRepository
{
    private readonly IDbConnectionFactory _connectionFactory;
    /// <summary>
    /// DatabasesRepository constructor
    /// </summary>
    public DatabasesRepository(IDbConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }

    /// <inheritdoc/>
    public async Task<IList<Domain.Database>> GetAllDatabasesAsync(string databaseName)
    {
        var query = @$"SELECT [Name] as [DatabaseName]
FROM [sys].[databases]
WHERE [Name] NOT IN ('master', 'tempdb', 'model', 'msdb')
AND [Name] = ISNULL('{databaseName}', [Name]);";

        using (var connection = _connectionFactory.GetDbConnection())
        {
            var databases = await connection.QueryAsync<Domain.Database>(query)
                .ConfigureAwait(false);

            return databases.ToList();
        }
    }
}
