using RATPSmartSystems.DatabaseCodeExtractor.Domain;
using RATPSmartSystems.DatabaseCodeExtractor.Gateways;

namespace RATPSmartSystems.DatabaseCodeExtractor.Tables;

/// <inheritdoc cref="ITablesService"/>
public class TablesService : ITablesService
{
    private readonly ITablesRepository _tablesRepository;
    private readonly IStorageGateway _storageGateway;

    /// <summary>
    /// TablesService constructor
    /// </summary>
    public TablesService(ITablesRepository tablesRepository,
        IStorageGateway storageGateway)
    {
        _tablesRepository = tablesRepository;
        _storageGateway = storageGateway;
    }

    /// <inheritdoc/>
    public Task<IList<Table>> ExtractTablesAsync(string databaseName)
    {
        return _tablesRepository.GetAllTablesAsync(databaseName);
    }

    /// <inheritdoc/>
    public async Task<bool> ExtractTablesToFileAsync(string destinationPath, string databaseName)
    {
        var tables = await ExtractTablesAsync(databaseName)
            .ConfigureAwait(false);
        var result = true;
        const string tableFolder = "02-tables";
        const string foreignKeyFolder = "03-foreign-keys";
        foreach (var t in tables)
        {
            var dbName = t.DatabaseName.ToLowerInvariant();
            var commonRootFileName = $"{t.SchemaName}.{t.TableName}";
            var path = Path.Combine(destinationPath, dbName, tableFolder , commonRootFileName + ".sql");

            result &= await _storageGateway.SaveContent(path, t.ToString())
                .ConfigureAwait(false);
            var foreignKeyScript = t.ForeignKeyScript;

            if (!string.IsNullOrEmpty(foreignKeyScript))
            {
                var foreignKeyPath = Path.Combine(destinationPath, dbName, foreignKeyFolder, commonRootFileName + ".fk.sql");
                result &= await _storageGateway.SaveContent(foreignKeyPath, foreignKeyScript )
                    .ConfigureAwait(false);
            }
        }

        return result;
    }
}
