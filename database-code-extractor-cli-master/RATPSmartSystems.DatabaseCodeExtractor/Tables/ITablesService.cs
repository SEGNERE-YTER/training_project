using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Tables;

/// <summary>
/// Provides extraction methods for tables
/// </summary>
public interface ITablesService
{
    /// <summary>
    /// Extracts all tables from a database
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>The list containing the tables info</returns>
    Task<IList<Table>> ExtractTablesAsync(string databaseName);

    /// <summary>
    /// Extracts all tables from a database and stores it into files
    /// </summary>
    /// <param name="destinationPath">The destination path for schema files</param>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>True if succeed; False otherwise</returns>
    Task<bool> ExtractTablesToFileAsync(string destinationPath, string databaseName);
}
