using System.Data;
using System.Runtime.CompilerServices;
using Dapper;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using RATPSmartSystems.DatabaseCodeExtractor.Domain;

[assembly: InternalsVisibleTo("RATPSmartSystems.DatabaseCodeExtractor.Tests")]
namespace RATPSmartSystems.DatabaseCodeExtractor.Tables;

/// <inheritdoc cref="ITablesRepository"/>
internal class TablesRepository : ITablesRepository
{
    private readonly IDbConnectionFactory _connectionFactory;
    /// <summary>
    /// TablesRepository constructor
    /// </summary>
    public TablesRepository(IDbConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }

    /// <inheritdoc/>
    public async Task<IList<Table>> GetAllTablesAsync(string databaseName)
    {
        var result = new List<Table>();
        try
        {
            async Task GetTablesByDatabaseAsync(string database)
            {
                var twoPartNames = await GetTableThreePartNamesAsync(database)
                    .ConfigureAwait(false);
                foreach (var f in twoPartNames)
                {
                    var table = await GetTableAsync(database, f.SchemaName, f.TableName)
                        .ConfigureAwait(false);
                    result.Add(table);
                }
            }

            if (!string.IsNullOrEmpty(databaseName))
            {
                await GetTablesByDatabaseAsync(databaseName)
                    .ConfigureAwait(false);
            }
            else
            {
                var dbNames = await GetDatabaseNamesAsync()
                    .ConfigureAwait(false);

                foreach (var dbName in dbNames)
                {
                    await GetTablesByDatabaseAsync(dbName)
                        .ConfigureAwait(false);
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

        return result;
    }

    private async Task<IList<string>> GetDatabaseNamesAsync()
    {
        var query = @"SELECT [Name] as [DatabaseName]
FROM [sys].[databases]
WHERE [Name] NOT IN ('master', 'tempdb', 'model', 'msdb');";

        using var connection = _connectionFactory.GetDbConnection();

        var databases = await connection.QueryAsync<string>(query)
            .ConfigureAwait(false);

        return databases.ToList();
    }

    private sealed class TableInfo
    {
        public string Item { get; set; } = string.Empty;

        public string ForeignKeys { get; set; } = string.Empty;
    }
    private async Task<Table> GetTableAsync(string databaseName, string schemaName,string tableName)
    {
        using var connection = _connectionFactory.GetDbConnection();

        connection.ChangeDatabase(databaseName);
        await connection.ExecuteAsync(TableCreationScript.ProcedureForTableCreationScript)
            .ConfigureAwait(false);
        var tableInfo = await connection.QuerySingleAsync<TableInfo>("[dbo].[GetTableCreationScript]",new {
            TBL = $"[{schemaName}].[{tableName}]"
        }, commandType: CommandType.StoredProcedure)
            .ConfigureAwait(false);

        var result = new Table
        {
            DatabaseName = databaseName,
            TableName = tableName,
            SchemaName = schemaName,
            CreationScript = tableInfo.Item,
            ForeignKeyScript = tableInfo.ForeignKeys
        };

        return result;
    }

    private async Task<IList<Table>> GetTableThreePartNamesAsync(string databaseName)
    {
        var result = new List<Table>();
        var query = @"SELECT OBJECT_NAME(object_id) AS [objectName]
, SCHEMA_NAME(schema_id) as [schemaName]
FROM [sys].[tables]";

        using var connection = _connectionFactory.GetDbConnection();

        connection.ChangeDatabase(databaseName);

        var tables = await connection.QueryAsync<(string, string)>(query)
            .ConfigureAwait(false);
        foreach (var (tableName, schemaName) in tables)
        {
            result.Add(new Table{TableName = tableName, SchemaName = schemaName, DatabaseName = databaseName});
        }

        return result;
        }
}
