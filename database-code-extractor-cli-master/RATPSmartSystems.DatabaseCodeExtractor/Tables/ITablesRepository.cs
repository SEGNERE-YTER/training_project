using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Tables;

/// <summary>
/// Provides persistence features about tables
/// </summary>
public interface ITablesRepository
{
    /// <summary>
    /// Gets all the tables from database
    /// </summary>
    /// <param name="databaseName">The name of the database</param>
    /// <returns>The list of the tables</returns>
    Task<IList<Table>> GetAllTablesAsync(string databaseName);
}
