using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Jobs;

/// <summary>
/// Provides extraction methods for jobs
/// </summary>
public interface IJobsService
{
    /// <summary>
    /// Extracts all jobs from a database
    /// </summary>
    /// <returns>The list containing the jobs info</returns>
    Task<IList<Job>> ExtractJobsAsync();

    /// <summary>
    /// Extracts all jobs from a database and stores it into files
    /// </summary>
    /// <param name="destinationPath">The destination path for job files</param>
    /// <returns>True if succeed; False otherwise</returns>
    Task<bool> ExtractToFileAsync(string destinationPath);
}
