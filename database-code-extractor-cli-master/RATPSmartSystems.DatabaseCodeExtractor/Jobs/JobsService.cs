using RATPSmartSystems.DatabaseCodeExtractor.Domain;
using RATPSmartSystems.DatabaseCodeExtractor.Gateways;

namespace RATPSmartSystems.DatabaseCodeExtractor.Jobs;

/// <inheritdoc cref="IJobsService"/>
internal class JobsService : IJobsService
{
    private readonly IJobsRepository _jobsRepository;
    private readonly IStorageGateway _storageGateway;

    /// <summary>
    /// JobsService constructor
    /// </summary>
    public JobsService(IJobsRepository jobsRepository,
        IStorageGateway storageGateway)
    {
        _jobsRepository = jobsRepository;
        _storageGateway = storageGateway;
    }

    /// <inheritdoc/>
    public Task<IList<Job>> ExtractJobsAsync()
    {
        return _jobsRepository.GetAllJobsAsync();
    }

    /// <inheritdoc/>
    public async Task<bool> ExtractToFileAsync(string destinationPath)
    {
        var jobs = await ExtractJobsAsync()
            .ConfigureAwait(false);
        var result = true;

        foreach (var j in jobs)
        {
            var path = Path.Combine(destinationPath, "jobs", $"{j.Name}.sql");
            result &= await _storageGateway.SaveContent(path, j.ToString())
                .ConfigureAwait(false);
        }

        return result;
    }
}
