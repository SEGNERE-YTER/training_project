using System.Runtime.CompilerServices;
using System.Text;
using Dapper;
using RATPSmartSystems.DatabaseCodeExtractor.Database;
using RATPSmartSystems.DatabaseCodeExtractor.Domain;

[assembly: InternalsVisibleTo("RATPSmartSystems.DatabaseCodeExtractor.Tests")]
namespace RATPSmartSystems.DatabaseCodeExtractor.Jobs;

/// <inheritdoc cref="IJobsRepository"/>
internal class JobsRepository : IJobsRepository
{
    private readonly IDbConnectionFactory _connectionFactory;

    /// <summary>
    /// JobsRepository constructor
    /// </summary>
    public JobsRepository(IDbConnectionFactory connectionFactory)
    {
        _connectionFactory = connectionFactory;
    }

    /// <inheritdoc/>
    public async Task<IList<Job>> GetAllJobsAsync()
    {
        var result = new List<Job>();

        try
        {
            var jobs = await GetAllJobsInfo().ConfigureAwait(false);
            result.AddRange(jobs);
            foreach (var j in result)
            {
                var jobSteps = await GetJobSteps(j.Id).ConfigureAwait(false);
                j.Steps = jobSteps;
                var schedules = await GetJobSchedules(j.Id).ConfigureAwait(false);
                j.Schedules = schedules;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

        return result;
    }

    private async Task<List<JobSchedule>> GetJobSchedules(Guid jobId)
    {
        var query = @$"USE [msdb]
SELECT
	[s].[name] AS [ScheduleName],
	[s].[enabled] AS [Enabled],
	[s].[freq_type] AS [FrequenceType],
	[s].[freq_interval] AS [FrequenceInterval],
	[s].[freq_subday_type] AS [FrequenceSubdayType],
	[s].[freq_subday_interval] AS [FrequenceSubdayInterval],
	[s].[freq_relative_interval] AS [FrequenceRelativeInterval],
	[s].[freq_recurrence_factor] AS [FrequenceRecurrenceFactor],
	[s].[active_start_date] AS [ActiveStartDate],
	[s].[active_end_date] AS [ActiveEndDate],
	[s].[active_start_time] AS [ActiveStartTime],
	[s].[active_end_time] AS [ActiveEndTime],
	[s].[schedule_uid] AS [ScheduleUid]
FROM [msdb].[dbo].[sysjobs_view] jv WITH (NOLOCK)
INNER JOIN [msdb].[dbo].[sysjobschedules] js WITH (NOLOCK)
ON [jv].[job_id] = [js].[job_id]
INNER JOIN [msdb].[dbo].[sysschedules] s WITH (NOLOCK)
ON [s].[schedule_id] = [js].[schedule_id]
WHERE [js].[job_id] = '{jobId}';";

        using var connection = _connectionFactory.GetDbConnection();
        var result = await connection.QueryAsync<JobSchedule>(query).ConfigureAwait(false);
        return result.ToList();
    }

    private async Task<List<JobStep>> GetJobSteps(Guid jobId)
    {
        var query = @$"USE [msdb]

SELECT [s].[step_name] AS [StepName],
[s].[step_id] AS [StepId],
[s].[command] AS [Command],
[s].[database_name] AS [DatabaseName],
[s].[subsystem] AS [SubSystem],
[s].[cmdexec_success_code] AS [CommandExecutionSuccessCode],
[s].[on_success_action] AS [OnSuccessAction],
[s].[on_success_step_id] AS [OnSuccessStepId],
[s].[on_fail_action] AS [OnFailAction],
[s].[on_fail_step_id] AS [OnFailStepId],
[s].[retry_attempts] AS [RetryAttempts],
[s].[retry_interval] AS [RetryInterval],
[s].[os_run_priority] AS [OperatingSystemRunPriority],
[s].[flags] AS [Flags]
FROM [dbo].[sysjobsteps] s WITH (NOLOCK)
WHERE job_id = '{jobId}'";

        using var connection = _connectionFactory.GetDbConnection();
        var result = (await connection.QueryAsync<JobStep>(query)
            .ConfigureAwait(false))
            .ToList();

        return result;
    }

    private async Task<IList<Job>> GetAllJobsInfo()
    {
        var query = @"USE [msdb]
SELECT [job_id] as [Id],
j.[name] as [Name],
j.[description] as [Description],
l.[name] as [Owner],
j.[enabled] as [Enabled],
c.[name] as [Category]
FROM [msdb].[dbo].[sysjobs] j WITH (NOLOCK)
LEFT JOIN [master].[sys].[syslogins] l WITH (NOLOCK) ON j.[owner_sid] = l.[sid]
JOIN [msdb].[dbo].[syscategories] c WITH (NOLOCK) ON j.[category_id] = c.[category_id];";

        using var connection = _connectionFactory.GetDbConnection();

        var jobs = await connection.QueryAsync<Job>(query)
            .ConfigureAwait(false);

        return jobs.ToList();
    }
}
