using RATPSmartSystems.DatabaseCodeExtractor.Domain;

namespace RATPSmartSystems.DatabaseCodeExtractor.Jobs;

/// <summary>
/// Provides persistence features for SQL jobs
/// </summary>
public interface IJobsRepository
{
    /// <summary>
    /// Gets all SQL jobs from a database
    /// </summary>
    /// <returns>The list of SQL jobs</returns>
    Task<IList<Job>> GetAllJobsAsync();
}
