namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Models a user-defined function
/// </summary>
public class Function
{
    /// <summary>
    /// The name of the database
    /// </summary>
    public string DatabaseName { get; set; } = string.Empty;

    /// <summary>
    /// The name of the function
    /// </summary>
    public string FunctionName { get; set; } = string.Empty;

    /// <summary>
    /// The name of the schema
    /// </summary>
    public string SchemaName { get; set; } = string.Empty;

    /// <summary>
    /// The content of the user-defined function
    /// </summary>
    public string Content { get; set; } = string.Empty;

    /// <inheritdoc/>
    public override string ToString()
    {
        return Content;
    }
}
