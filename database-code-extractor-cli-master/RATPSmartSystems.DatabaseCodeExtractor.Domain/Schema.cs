namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Models a database Schema
/// </summary>
public class Schema
{
    /// <summary>
    /// The name of the database
    /// </summary>
    public string DatabaseName { get; set; } = string.Empty;

    /// <summary>
    /// The name of the schema
    /// </summary>
    public string SchemaName { get; set; } = string.Empty;

    /// <summary>
    /// The owner of the schema
    /// </summary>
    public string SchemaOwner { get; set; } = string.Empty;

    /// <inheritdoc/>
    public override string ToString()
    {
        return @$"IF NOT EXISTS (SELECT * FROM [{DatabaseName}].[sys].[schemas] WHERE [name] = '{SchemaName}')
BEGIN
    EXEC('CREATE SCHEMA [{SchemaName}]')
END";
    }
}
