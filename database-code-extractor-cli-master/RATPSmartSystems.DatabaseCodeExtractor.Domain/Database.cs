namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Models database creation script
/// </summary>
public class Database
{
    /// <summary>
    /// The name of the database
    /// </summary>
    public string DatabaseName { get; set; } = string.Empty;

    /// <inheritdoc/>
    public override string ToString()
    {
        return @$"USE master
GO
IF NOT EXISTS (
SELECT [name]
FROM sys.databases
WHERE [name] = N'{DatabaseName}'
)
CREATE DATABASE [{DatabaseName}]
GO";
    }
}
