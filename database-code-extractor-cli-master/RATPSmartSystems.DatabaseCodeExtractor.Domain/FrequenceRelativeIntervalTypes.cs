namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Enumerates frequence relative interval types
/// </summary>
public enum FrequenceRelativeIntervalTypes
{
    /// <summary>
    /// freq_relative_interval is unused
    /// </summary>
    Unused = 0,
    /// <summary>
    /// First
    /// </summary>
    First = 1,
    /// <summary>
    /// Second
    /// </summary>
    Second = 2,
    /// <summary>
    /// Third
    /// </summary>
    Third = 4,
    /// <summary>
    /// Fourth
    /// </summary>
    Fourth = 8,
    /// <summary>
    /// Last
    /// </summary>
    Last = 16

}
