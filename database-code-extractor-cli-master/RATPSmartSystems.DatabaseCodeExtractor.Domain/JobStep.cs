namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Models a step of a job
/// </summary>
public class JobStep
{
    /// <summary>
    /// The name of the step
    /// </summary>
    public string StepName { get; set; } = string.Empty;

    /// <summary>
    /// The idntifier of the step in the job
    /// </summary>
    public int StepId { get; set; } = -1;

    /// <summary>
    /// The content of the command
    /// </summary>
    public string Command { get; set; } = string.Empty;

    /// <summary>
    /// The name of the associated database
    /// </summary>
    public string DatabaseName { get; set; } = string.Empty;

    /// <summary>
    /// The associated subsystem
    /// </summary>
    public JobSubSystems SubSystem { get; set; } = JobSubSystems.TSQL;

    /// <summary>
    /// Command execution success code
    /// </summary>
    public int CommandExecutionSuccessCode { get; set; } = -1;

    /// <summary>
    /// On success action
    /// </summary>
    public int OnSuccessAction { get; set; } = -1;

    /// <summary>
    /// On success step identifier
    /// </summary>
    public int OnSuccessStepId { get; set; } = -1;

    /// <summary>
    /// On fail action
    /// </summary>
    public int OnFailAction { get; set; } = -1;

    /// <summary>
    /// On fail step identifier
    /// </summary>
    public int OnFailStepId { get; set; } = -1;

    /// <summary>
    /// Retry attempts
    /// </summary>
    public int RetryAttempts { get; set; } = -1;

    /// <summary>
    /// Retry interval
    /// </summary>
    public int RetryInterval { get; set; } = -1;

    /// <summary>
    /// Operating system run priority
    /// </summary>
    public int OperatingSystemRunPriority { get; set; } = -1;

    /// <summary>
    /// Flags
    /// </summary>
    public int Flags { get; set; } = -1;

    /// <inheritdoc/>
    public override string ToString()
    {
	    var step =
		    $@"EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'{StepName}',
@step_id={StepId},
@cmdexec_success_code={CommandExecutionSuccessCode},
@on_success_action={OnSuccessAction},
@on_success_step_id={OnSuccessStepId},
@on_fail_action={OnFailAction},
@on_fail_step_id={OnFailStepId},
@retry_attempts={RetryAttempts},
@retry_interval={RetryInterval},
@os_run_priority={OperatingSystemRunPriority},
@subsystem=N'{SubSystem.ToString()}',
@command=N'{Command}',
@database_name=N'{DatabaseName}',
@flags={Flags}
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback";

	    return step;
    }
}
