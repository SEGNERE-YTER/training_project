namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Enumerates the different kinds of frequence for job schedules
/// </summary>
public enum ScheduleFrequenceTypes
{
    /// <summary>
    /// One time only
    /// </summary>
    OneTimeOnly = 1,
    /// <summary>
    /// Daily
    /// </summary>
    Daily = 4,
    /// <summary>
    /// Weekly
    /// </summary>
    Weekly = 8,
    /// <summary>
    /// Monthly
    /// </summary>
    Monthly = 16,
    /// <summary>
    /// Monthly, relative to freq_interval
    /// </summary>
    RelativeMonthly = 32,
    /// <summary>
    /// Runs when the SQL Server Agent service starts
    /// </summary>
    AgentTriggered = 64,
    /// <summary>
    /// Runs when the computer is idle
    /// </summary>
    ComputerIdle = 128

}
