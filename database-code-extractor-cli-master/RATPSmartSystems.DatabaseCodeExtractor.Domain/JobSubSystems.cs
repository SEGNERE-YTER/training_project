namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Enumerates job subsystems
/// </summary>
public enum JobSubSystems
{
    TSQL = 0,
    CmdExec = 1,
    Snapshot = 2,
    LogReader = 3,
    Distribution = 4,
    Merge = 5,
    QueueReader = 6,
    ANALYSISQUERY = 7,
    ANALYSISCOMMAND = 8,
    SSIS = 9,
    PowerShell = 10

}
