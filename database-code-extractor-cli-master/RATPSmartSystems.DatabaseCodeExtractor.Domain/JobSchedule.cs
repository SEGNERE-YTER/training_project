namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Models the scheduling of a job
/// </summary>
public class JobSchedule
{
    /// <summary>
    /// The name of the job schedule
    /// </summary>
    public string ScheduleName { get; set; } = string.Empty;

    /// <summary>
    /// Determines whether the job schedule is enabled or not
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Determines how frequently a job runs for this schedule
    /// </summary>
    public ScheduleFrequenceTypes FrequenceType { get; set; }

    /// <summary>
    /// Days that the job is executed
    /// Depends on the value of freq_type
    /// The default value is 0, which indicates that freq_interval is unused
    /// See the table below for the possible values and their effects
    /// </summary>
    public int FrequenceInterval { get; set; }

    /// <summary>
    /// Units for the freq_subday_interval
    /// </summary>
    public ScheduleFrequenceSubdayTypes FrequenceSubdayType { get; set; }

    /// <summary>
    /// Number of freq_subday_type periods to occur between each execution of the job
    /// </summary>
    public int FrequenceSubdayInterval { get; set; }

    /// <summary>
    /// When freq_interval occurs in each month, if freq_type is 32 (monthly relative)
    /// </summary>
    public FrequenceRelativeIntervalTypes FrequenceRelativeInterval { get; set; }

    /// <summary>
    /// Number of weeks or months between the scheduled execution of a job
    /// freq_recurrence_factor is used only if freq_type is 8, 16, or 32
    /// If this column contains 0, freq_recurrence_factor is unused.
    /// </summary>
    public int FrequenceRecurrenceFactor { get; set; }

    /// <summary>
    /// Date on which execution of a job can begin
    /// The date is formatted as YYYYMMDD
    /// NULL indicates today's date.
    /// </summary>
    public int ActiveStartDate { get; set; }

    /// <summary>
    /// Date on which execution of a job can stop
    /// The date is formatted YYYYMMDD
    /// </summary>
    public int ActiveEndDate { get; set; }

    /// <summary>
    /// Time on any day between active_start_date and active_end_date that job begins executing
    /// Time is formatted HHMMSS, using a 24-hour clock
    /// </summary>
    public int ActiveStartTime { get; set; }

    /// <summary>
    /// Time on any day between active_start_date and active_end_date that job stops executing
    /// Time is formatted HHMMSS, using a 24-hour clock.
    /// </summary>
    public int ActiveEndTime { get; set; }

    /// <summary>
    /// The unique identifier of the job schedule
    /// </summary>
    public Guid ScheduleUid { get; set; }

    /// <inheritdoc/>/>
    public override string ToString()
    {
        var schedule = $@"EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'{ScheduleName}',
@enabled={Enabled},
@freq_type={FrequenceType},
@freq_interval={FrequenceInterval},
@freq_subday_type={FrequenceSubdayType},
@freq_subday_interval={FrequenceSubdayInterval},
@freq_relative_interval={(int)FrequenceRelativeInterval},
@freq_recurrence_factor={FrequenceRecurrenceFactor},
@active_start_date={ActiveStartDate},
@active_end_date={ActiveEndDate},
@active_start_time={ActiveStartTime},
@active_end_time={ActiveEndTime},
@schedule_uid=N'{ScheduleUid}'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback";

        return schedule;
    }
}
