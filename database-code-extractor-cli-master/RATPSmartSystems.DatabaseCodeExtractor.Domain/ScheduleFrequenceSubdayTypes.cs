namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Enumerates units for the freq_subday_interval
/// </summary>
public enum ScheduleFrequenceSubdayTypes
{
    /// <summary>
    /// At the specified time
    /// </summary>
    AtSpecifiedTime = 1,
    /// <summary>
    /// Seconds
    /// </summary>
    Seconds = 2,
    /// <summary>
    /// Minutes
    /// </summary>
    Minutes = 4,
    /// <summary>
    /// Hours
    /// </summary>
    Hours = 8

}
