namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Models a database table
/// </summary>
public class Table
{
    /// <summary>
    /// The name of the database
    /// </summary>
    public string DatabaseName { get; set; } = string.Empty;

    /// <summary>
    /// The name of the table
    /// </summary>
    public string TableName { get; set; } = string.Empty;

    /// <summary>
    /// The name of the schema
    /// </summary>
    public string SchemaName { get; set; } = string.Empty;

    /// <summary>
    /// The creation script
    /// </summary>
    public string CreationScript { get; set; } = string.Empty;

    public string ForeignKeyScript { get; set; } = string.Empty;

    /// <inheritdoc/>
    public override string ToString()
    {
        return CreationScript;
    }
}
