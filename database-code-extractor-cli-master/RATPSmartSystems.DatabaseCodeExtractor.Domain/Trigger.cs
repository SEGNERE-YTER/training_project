namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Models a trigger
/// </summary>
public class Trigger
{
    /// <summary>
    /// The name of the database
    /// </summary>
    public string DatabaseName { get; set; } = string.Empty;

    /// <summary>
    /// The name of the trigger
    /// </summary>
    public string TriggerName { get; set; } = string.Empty;

    /// <summary>
    /// The name of the schema
    /// </summary>
    public string SchemaName { get; set; } = string.Empty;

    /// <summary>
    /// The content of the trigger
    /// </summary>
    public string Content { get; set; } = string.Empty;

    /// <inheritdoc/>
    public override string ToString()
    {
        return Content;
    }
}
