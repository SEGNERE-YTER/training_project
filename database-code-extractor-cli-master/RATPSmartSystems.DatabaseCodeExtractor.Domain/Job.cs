﻿using System.Text;

namespace RATPSmartSystems.DatabaseCodeExtractor.Domain;

/// <summary>
/// Models an SQL job
/// </summary>
public class Job
{
	/// <summary>
	/// The identifier of the job
	/// </summary>
	public Guid Id { get; set; }

    /// <summary>
    /// The name of the job
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// The description of the job
    /// </summary>
    public string Description { get; set; } = string.Empty;

    /// <summary>
    /// The owner of the job
    /// </summary>
    public string Owner { get; set; } = string.Empty;

    /// <summary>
    /// List of steps contained in the job
    /// </summary>
    public List<JobStep> Steps { get; set; } = new();

    /// <summary>
    /// List of schedules info of the job
    /// </summary>
    public List<JobSchedule> Schedules { get; set; } = new();

    /// <summary>
    /// Determines whether the job is enabled or not
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// The job category
    /// </summary>
    public string Category { get; set; } = string.Empty;

    /// <inheritdoc/>
    public override string ToString()
    {
	    var steps = Steps.Aggregate(string.Empty, (current, step) => current + new StringBuilder(step.ToString()).AppendLine());
        var schedules = Schedules.Aggregate(string.Empty, (current, schedule) => current + new StringBuilder(schedule.ToString()).AppendLine());
	    return $@"USE [msdb]
GO

BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[{Category}]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[{Category}]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'{Name}',
		@enabled={Convert.ToInt32(Enabled)},
		@notify_level_eventlog=0,
		@notify_level_email=0,
		@notify_level_netsend=0,
		@notify_level_page=0,
		@delete_level=0,
		@description=N'{Description}',
		@category_name=N'[{Category}]',
		@owner_login_name=N'{Owner}', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
{steps}
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
{schedules}
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO
";
    }
}
