namespace RATPSmartSystems.DatabaseCodeExtractor.Gateways;

/// <inheritdoc cref="IStorageGateway"/>
public class StorageGateway : IStorageGateway
{
    /// <inheritdoc/>
    public async Task<bool> SaveContent(string filePath, string content)
    {
        bool result;
        try
        {
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(Path.GetDirectoryName(filePath)!);

            await File.WriteAllTextAsync(filePath, content).ConfigureAwait(false);
            result = true;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            result = false;
        }

        return result;

    }
}
