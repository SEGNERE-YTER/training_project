namespace RATPSmartSystems.DatabaseCodeExtractor.Gateways;

/// <summary>
/// Provides features about file storage
/// </summary>
public interface IStorageGateway
{
    /// <summary>
    /// Saves a content into a file
    /// </summary>
    /// <param name="filePath">The path of the file to be saved</param>
    /// <param name="content">The content to be saved</param>
    /// <returns>True if succeed; False otherwise</returns>
    Task<bool> SaveContent(string filePath, string content);
}
