# database-code-extractor-cli

Hi there :wave:

Welcome to **database-code-extractor-cli** !

database-code-extractor-cli is a command line tool that is able extract every single code that your SQL Server database contains.

## :sparkles: Prerequisites

In order to contribute to this project, please install the following tools:

* [nodejs](https://nodejs.org/en/download/)
* [docker](https://docs.docker.com/engine/install/)
* [dotnet](https://dotnet.microsoft.com/en-us/download)

> The version of NodeJS should be 12.X.X or higher

Then, it is **mandatory** to execute the following command prior to any other action:

```bash
npm i
```

Using this command will help you to install [**husky**](https://typicode.github.io/husky/#/) and its [**git hooks**](https://githooks.com/).
Basically, it will ensures that we :rocket: and avoid :shit:

## :tada: Getting Started

The **database-code-extractor-cli** provides the following features:

* Extracts **databases**
* Extracts **functions**
* Extracts **jobs** 
* Extracts **procedures**
* Extracts **schemas**
* Extracts **tables**
* Extracts **triggers**

It does not extract views, **yet**.
Also, it does not extract permissions or server links and it is not planned.

### help

```bash
./db-extractor --help
```

### extract

```bash
./db-extractor --filter Procedures --connection-string "Server=localhost, 1433;Database=master;User Id=sa;Password=password123!" --output-path <where/you/want/to/generate/code>
```

### :construction_worker: How to build

To build the **database-code-extractor-cli**, grab your favorite terminal and execute the following commands:

```bash
# first, go to the right directory 
cd /where/you/git/cloned/this/repository
# if your os is linux-based, you can execute as follow
dotnet publish --configuration Release --runtime linux-x64 --self-contained true
# if your os is windows 10 or later, you can execute as follow
# go further with runtime identifiers here => https://docs.microsoft.com/en-us/dotnet/core/rid-catalog#using-rids
dotnet publish --configuration Release --runtime win10-x64 --self-contained true
# as an option, you can build the application where you want:
dotnet publish --configuration Release --runtime win10-x64 --self-contained true --output /where/you/want
```

### :white_check_mark: How to execute tests

This project comes with some tests for the **RATPSmartSystems.DatabaseCodeExtractor** library (where all the magic happens).

In the **RATPSmartSystems.DatabaseCodeExtractor.Tests** project, we wrote two levels of tests: 

* Unit tests
* Integration tests (with a dockerized database)

In order to setup your testing database, you just need to:

```bash
cd /root/of/this/repository
docker compose up
```

Then, you should be able to execute all tests as follow:

```bash
cd RATPSmartSystems.DatabaseCodeExtractor.Tests
dotnet test
# if you want to execute a specific test, just do:
dotnet test --filter GivenValidDatabaseWhenGetAllTablesThenSucceed
# GivenValidDatabaseWhenGetAllTablesThenSucceed is just an example
```

### :building_construction: How is it designed

The design of **database-code-extractor-cli** is described [here](./ARCHITECTURE.md)


## Contributing

### Contributing guidelines

Contributing guidelines are not **yet** written!

Please be patient :pray:

## Code of Conduct

Help us keep Navocap open and inclusive. Please read and follow our [Code of Conduct](./CODE_OF_CONDUCT.md)