namespace RATPSmartSystems.DatabaseCodeExtractor.Console;

/// <summary>
/// Defines the different kinds of object that are extractable from the database
/// </summary>
public enum ExtractionFilter
{
    /// <summary>
    /// Gets every creation scripts from a database server
    /// </summary>
    All,
    /// <summary>
    /// Gets the creation scripts for databases
    /// </summary>
    Databases,
    /// <summary>
    /// Gets the creation scripts for schemas
    /// </summary>
    Schemas,
    /// <summary>
    /// Gets the creation scripts for permissions
    /// </summary>
    Permissions,
    /// <summary>
    /// Gets the creation scripts for tables
    /// </summary>
    Tables,
    /// <summary>
    /// Gets the creation scripts for procedures
    /// </summary>
    Procedures,
    /// <summary>
    /// Gets the creation scripts for functions
    /// </summary>
    Functions,
    /// <summary>
    /// Gets the creation scripts for triggers
    /// </summary>
    Triggers,
    /// <summary>
    /// Gets the creation scripts for jobs
    /// </summary>
    Jobs,
    /// <summary>
    /// Gets the creation scripts for server links
    /// </summary>
    ServerLinks
}
