﻿namespace RATPSmartSystems.DatabaseCodeExtractor.Console;

internal static class Program
{
        /// <summary>
        /// db-extractor is a tool that helps you to extract the code from your SQLServer database
        /// </summary>
        /// <param name="filter">Filters the database objects to be extracted</param>
        /// <param name="connectionString">The database connection string</param>
        /// <param name="outputPath">The output path where the files will be generated</param>
        public static Task Main(
            ExtractionFilter filter,
            string connectionString,
            string outputPath = ".")
        {
            try
            {
                if (string.IsNullOrEmpty(connectionString))
                    throw new ArgumentException("Missing required parameter: connectionString");

                return ExecuteExtractCommandAsync(filter, connectionString, outputPath);
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
                return Task.FromResult(false);
            }
        }

        private static async Task<bool> ExecuteExtractCommandAsync(ExtractionFilter filter, string connectionString, string outputPath)
        {
            var dbConnectionFactory = DatabaseCodeExtractorFactory.CreateDbConnectionFactory(connectionString);
            var databaseName = connectionString.Split(';').FirstOrDefault(x => x.Contains("Database"))?.Split('=')[1]!;

            var result = filter switch
            {
                ExtractionFilter.Databases => await DatabaseCodeExtractorFactory
                    .CreateDatabasesService(dbConnectionFactory)
                    .ExtractDatabasesToFileAsync(outputPath, databaseName)
                    .ConfigureAwait(false),
                ExtractionFilter.Schemas => await DatabaseCodeExtractorFactory.CreateSchemasService(dbConnectionFactory)
                    .ExtractSchemasToFileAsync(outputPath, databaseName)
                    .ConfigureAwait(false),
                ExtractionFilter.Tables => await DatabaseCodeExtractorFactory.CreateTablesService(dbConnectionFactory)
                    .ExtractTablesToFileAsync(outputPath, databaseName)
                    .ConfigureAwait(false),
                ExtractionFilter.Procedures => await DatabaseCodeExtractorFactory
                    .CreateProceduresService(dbConnectionFactory)
                    .ExtractProceduresToFileAsync(outputPath, databaseName)
                    .ConfigureAwait(false),
                ExtractionFilter.Functions => await DatabaseCodeExtractorFactory
                    .CreateFunctionsService(dbConnectionFactory)
                    .ExtractFunctionsToFileAsync(outputPath, databaseName)
                    .ConfigureAwait(false),
                ExtractionFilter.Triggers => await DatabaseCodeExtractorFactory.CreateTriggersService(dbConnectionFactory)
                    .ExtractTriggersToFileAsync(outputPath, databaseName)
                    .ConfigureAwait(false),
                ExtractionFilter.Jobs => await DatabaseCodeExtractorFactory.CreateJobsService(dbConnectionFactory)
                    .ExtractToFileAsync(outputPath)
                    .ConfigureAwait(false),
                _ => await ExtractAllAsync(connectionString, outputPath, databaseName)
                    .ConfigureAwait(false)
            };

            System.Console.WriteLine($"db-extractor {(result ? string.Empty : "did not")} succeed to extract {filter.ToString().ToLowerInvariant()} into {outputPath}");

            return result;
        }

        private static async Task<bool> ExtractAllAsync(string connectionString, string outputPath, string databaseName)
        {
            var dbConnectionFactory = DatabaseCodeExtractorFactory.CreateDbConnectionFactory(connectionString);

            var result = await DatabaseCodeExtractorFactory.CreateDatabasesService(dbConnectionFactory)
                .ExtractDatabasesToFileAsync(outputPath, databaseName)
                .ConfigureAwait(false);
            result &= await DatabaseCodeExtractorFactory.CreateSchemasService(dbConnectionFactory)
                .ExtractSchemasToFileAsync(outputPath, databaseName)
                .ConfigureAwait(false);
            result &= await DatabaseCodeExtractorFactory.CreateTablesService(dbConnectionFactory)
                .ExtractTablesToFileAsync(outputPath, databaseName)
                .ConfigureAwait(false);
            result &= await DatabaseCodeExtractorFactory.CreateProceduresService(dbConnectionFactory)
                .ExtractProceduresToFileAsync(outputPath, databaseName)
                .ConfigureAwait(false);
            result &= await DatabaseCodeExtractorFactory.CreateFunctionsService(dbConnectionFactory)
                .ExtractFunctionsToFileAsync(outputPath, databaseName)
                .ConfigureAwait(false);
            result &= await DatabaseCodeExtractorFactory.CreateTriggersService(dbConnectionFactory)
                .ExtractTriggersToFileAsync(outputPath, databaseName)
                .ConfigureAwait(false);
            result &= await DatabaseCodeExtractorFactory.CreateJobsService(dbConnectionFactory)
                .ExtractToFileAsync(outputPath)
                .ConfigureAwait(false);
            return result;
        }
}
